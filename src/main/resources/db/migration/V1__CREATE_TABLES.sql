CREATE TABLE IF NOT EXISTS meal
(
    id     uuid        NOT NULL PRIMARY KEY,
    name   VARCHAR(32) NOT NULL,
    origin VARCHAR(32)
);

CREATE TABLE IF NOT EXISTS ingredient
(
    id      UUID        NOT NULL PRIMARY KEY,
    name    VARCHAR(32) NOT NULL CHECK (name <> ''),
    origin  VARCHAR(32),
    "type"  VARCHAR(32) NOT NULL CHECK (type <> ''),
    price   numeric     NOT NULL,
    meal_id uuid        NOT NULL REFERENCES meal (id)
);
