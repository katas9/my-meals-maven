package fr.dl.mymeal.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.lang.Nullable;

@Entity
@Table(name = "ingredient")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IngredientEntity {
    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue(generator = "uuid2")
    private UUID id;


    private String name;

    @Nullable
    private String type;

    @NotNull
    private double price;

    @NotNull
    private String origin;
}
