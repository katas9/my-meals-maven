package fr.dl.mymeal.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.lang.Nullable;

@Entity
@Table(name = "meal")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MealEntity {
    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue(generator = "uuid2")
    private UUID id;

    @NotNull
    private String name;

    @Nullable
    private String origin;

    private double price;
}
