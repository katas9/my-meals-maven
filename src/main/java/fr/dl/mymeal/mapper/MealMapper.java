package fr.dl.mymeal.mapper;

import fr.dl.mymeal.dto.MealDto;
import fr.dl.mymeal.model.MealEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import static org.mapstruct.ReportingPolicy.*;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface MealMapper {

    MealMapper MEAL_MAPPER = Mappers.getMapper(MealMapper.class);

    MealEntity toEntity(MealDto dto);

    MealDto toDto(MealEntity entity);
}
