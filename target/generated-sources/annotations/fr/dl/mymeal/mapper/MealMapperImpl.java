package fr.dl.mymeal.mapper;

import fr.dl.mymeal.dto.MealDto;
import fr.dl.mymeal.dto.MealDto.MealDtoBuilder;
import fr.dl.mymeal.model.MealEntity;
import fr.dl.mymeal.model.MealEntity.MealEntityBuilder;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-03-21T17:04:36+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_322 (Temurin)"
)
public class MealMapperImpl implements MealMapper {

    @Override
    public MealEntity toEntity(MealDto dto) {
        if ( dto == null ) {
            return null;
        }

        MealEntityBuilder mealEntity = MealEntity.builder();

        mealEntity.id( dto.getId() );
        mealEntity.name( dto.getName() );
        mealEntity.origin( dto.getOrigin() );
        mealEntity.price( dto.getPrice() );

        return mealEntity.build();
    }

    @Override
    public MealDto toDto(MealEntity entity) {
        if ( entity == null ) {
            return null;
        }

        MealDtoBuilder mealDto = MealDto.builder();

        mealDto.id( entity.getId() );
        mealDto.name( entity.getName() );
        mealDto.origin( entity.getOrigin() );
        mealDto.price( entity.getPrice() );

        return mealDto.build();
    }
}
